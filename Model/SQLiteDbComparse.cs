﻿using System;
using Dos.ORM;

namespace Dasic.Model
{
    /// <summary>
    ///     实体类SQLiteDbComparse。(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Table("dbcomparse")]
    [Serializable]
    public class SQLiteDbComparse : Entity
    {
        #region _Field

        /// <summary>
        ///     字段信息
        /// </summary>
        public class _
        {
            /// <summary>
            ///     *
            /// </summary>
            public static readonly Field All = new Field("*", "dbcomparse");

            /// <summary>
            /// </summary>
            public static readonly Field id = new Field("id", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field mip = new Field("mip", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field muser = new Field("muser", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field mpass = new Field("mpass", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field mport = new Field("mport", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field mtable = new Field("mtable", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field sip = new Field("sip", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field suser = new Field("suser", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field spass = new Field("spass", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field sport = new Field("sport", "dbcomparse", "");

            /// <summary>
            /// </summary>
            public static readonly Field stable = new Field("stable", "dbcomparse", "");
        }

        #endregion

        #region Model

        private int _id;
        private string _mip;
        private string _muser;
        private string _mpass;
        private int _mport;
        private string _mtable;
        private string _sip;
        private string _suser;
        private string _spass;
        private int _sport;
        private string _stable;

        /// <summary>
        /// </summary>
        [Field("id")]
        public int id
        {
            get { return _id; }
            set
            {
                OnPropertyValueChange("id");
                _id = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("mip")]
        public string mip
        {
            get { return _mip; }
            set
            {
                OnPropertyValueChange("mip");
                _mip = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("muser")]
        public string muser
        {
            get { return _muser; }
            set
            {
                OnPropertyValueChange("muser");
                _muser = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("mpass")]
        public string mpass
        {
            get { return _mpass; }
            set
            {
                OnPropertyValueChange("mpass");
                _mpass = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("mport")]
        public int mport
        {
            get { return _mport; }
            set
            {
                OnPropertyValueChange("mport");
                _mport = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("mtable")]
        public string mtable
        {
            get { return _mtable; }
            set
            {
                OnPropertyValueChange("mtable");
                _mtable = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("sip")]
        public string sip
        {
            get { return _sip; }
            set
            {
                OnPropertyValueChange("sip");
                _sip = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("suser")]
        public string suser
        {
            get { return _suser; }
            set
            {
                OnPropertyValueChange("suser");
                _suser = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("spass")]
        public string spass
        {
            get { return _spass; }
            set
            {
                OnPropertyValueChange("spass");
                _spass = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("sport")]
        public int sport
        {
            get { return _sport; }
            set
            {
                OnPropertyValueChange("sport");
                _sport = value;
            }
        }

        /// <summary>
        /// </summary>
        [Field("stable")]
        public string stable
        {
            get { return _stable; }
            set
            {
                OnPropertyValueChange("stable");
                _stable = value;
            }
        }

        #endregion

        #region Method

        /// <summary>
        ///     获取实体中的主键列
        /// </summary>
        public override Field[] GetPrimaryKeyFields()
        {
            return new[]
            {
                _.id
            };
        }

        /// <summary>
        ///     获取实体中的标识列
        /// </summary>
        public override Field GetIdentityField()
        {
            return _.id;
        }

        /// <summary>
        ///     获取列信息
        /// </summary>
        public override Field[] GetFields()
        {
            return new[]
            {
                _.id,
                _.mip,
                _.muser,
                _.mpass,
                _.mport,
                _.mtable,
                _.sip,
                _.suser,
                _.spass,
                _.sport,
                _.stable
            };
        }

        /// <summary>
        ///     获取值信息
        /// </summary>
        public override object[] GetValues()
        {
            return new object[]
            {
                _id,
                _mip,
                _muser,
                _mpass,
                _mport,
                _mtable,
                _sip,
                _suser,
                _spass,
                _sport,
                _stable
            };
        }

        /// <summary>
        ///     是否是v1.10.5.6及以上版本实体。
        /// </summary>
        /// <returns></returns>
        public override bool V1_10_5_6_Plus()
        {
            return true;
        }

        #endregion
    }
}